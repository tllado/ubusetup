#!/bin/bash

# Install Sublime 3
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sudo apt-get update
sudo apt-get install sublime-text

# Install utilities
sudo apt-get install -y git htop numlockx terminator vim

# Configure vim
touch ~/.vimrc
echo "set tabstop=4" >> ~/.vimrc
echo "set shiftwidth=4" >> ~/.vimrc
echo "set expandtab" >> ~/.vimrc
echo "set autoindent" >> ~/.vimrc
echo "set cindent" >> ~/.vimrc

# Upgrade Python
sudo apt install -y python-pip
pip install --upgrade pip
python -m pip install --user numpy scipy matplotlib ipython jupyter pandas sympy nose
